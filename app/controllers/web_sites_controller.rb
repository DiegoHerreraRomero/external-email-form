class WebSitesController < ApplicationController
  before_action :authenticate_user!, except: [:preview, :send_mail]
  skip_before_action :verify_authenticity_token, only: [:preview, :send_mail]
  before_action :set_web_site, except: [:preview, :send_mail]


  def new
    @templates = Template.all
  end

  def create
    @web_site = current_user.web_sites.new(web_site_params)
    respond_to do |format|
      if @web_site.save
        flash[:success] = "Guardado correctamente"
        format.html{ redirect_to root_path }
      else
        format.html{ render :new }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @web_site.update_attributes(web_site_params)
        flash[:success] = "Guardado correctamente"
        format.html{ redirect_to root_path }
      else
        format.html{ render :new }
      end
    end
  end

  def destroy
    respond_to do |format|
      if @web_site.destroy
        flash[:success] = "Eliminado correctamente"
        format.html{ redirect_to root_path }
      else
        format.html{ render :index }
      end
    end
  end

  def preview
    web_site = WebSite.find_by(hmac: params[:id]) rescue nil
    @id = params[:id]
    @js = web_site.js rescue params[:js]
    @css = web_site.css rescue params[:css]
    @html = web_site.html rescue params[:html]
    @framework = web_site.framework rescue (Framework.find(params[:framework]) rescue nil)
    render layout: false
  end

  def send_mail
    web_site = WebSite.find_by(hmac: params[:id]) rescue nil
    parameters = params.except(:controller, :action, :id)
    PreviewMailer.send_mail(web_site.email, parameters).deliver_now
    render plain: t(:message_send)
  end

  private
  def set_web_site
    @web_site = WebSite.find(params[:id]) rescue WebSite.new
  end

  def web_site_params
    params.require(:web_site).permit(
      :name,
      :email,
      :description,
      :css,
      :html,
      :js,
      :template_id,
      :framework_id
    )
  end

end
