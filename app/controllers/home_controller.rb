class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
    @web_sites = current_user.web_sites.order(id: :asc)
  end

end
