class PreviewMailer < ActionMailer::Base

  def send_mail(email, parameters)
    @parameters = parameters
    mail(to: email, subject: t(:notification_new_message))
  end
end
