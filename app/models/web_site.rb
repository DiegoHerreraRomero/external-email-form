class WebSite < ApplicationRecord

  belongs_to :user
  belongs_to :template, optional: true
  belongs_to :framework, optional: true

  validates :name, :email, presence: true

  after_create :create_hmac

  def create_hmac
    key = Rails.application.credentials.secret_key_base
    data = self.id.to_s
    self.hmac = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), key, data)
    self.save
  end


end