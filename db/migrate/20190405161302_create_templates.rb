class CreateTemplates < ActiveRecord::Migration[5.2]
  def change
    create_table :templates do |t|
      t.string :name, null: false
      t.references :framework, index: true, foreign_key: true
      t.text :css
      t.text :html
      t.text :js
    end
  end
end
