class CreateFrameworks < ActiveRecord::Migration[5.2]
  def change
    create_table :frameworks do |t|
      t.string :name, null: false
      t.string :url_info
      t.text :css
      t.text :js
    end
  end
end
