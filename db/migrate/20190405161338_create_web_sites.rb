class CreateWebSites < ActiveRecord::Migration[5.2]
  def change
    create_table :web_sites do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.string :email
      t.text :description, null: true
      t.references :template, index: true, foreign_key: true
      t.references :framework, index: true, foreign_key: true
      t.text :css, null: true
      t.text :html, null: true
      t.text :js, null: true
      t.string :hmac
      t.timestamps
    end
  end
end
