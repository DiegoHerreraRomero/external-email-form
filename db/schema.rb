# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_05_161338) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "frameworks", force: :cascade do |t|
    t.string "name", null: false
    t.string "url_info"
    t.text "css"
    t.text "js"
  end

  create_table "templates", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "framework_id"
    t.text "css"
    t.text "html"
    t.text "js"
    t.index ["framework_id"], name: "index_templates_on_framework_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "web_sites", force: :cascade do |t|
    t.bigint "user_id"
    t.string "name"
    t.string "email"
    t.text "description"
    t.bigint "template_id"
    t.bigint "framework_id"
    t.text "css"
    t.text "html"
    t.text "js"
    t.string "hmac"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["framework_id"], name: "index_web_sites_on_framework_id"
    t.index ["template_id"], name: "index_web_sites_on_template_id"
    t.index ["user_id"], name: "index_web_sites_on_user_id"
  end

  add_foreign_key "templates", "frameworks"
  add_foreign_key "web_sites", "frameworks"
  add_foreign_key "web_sites", "templates"
  add_foreign_key "web_sites", "users"
end
