# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

p 'Create frameworks'
frameworks = [
  {
    id: 1, 
    name: 'Bootstrap 4.3.1',
    css: '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">', 
    js: '<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>'
  },
  {
    id: 2, 
    name: 'Bulma 0.7.4', 
    css: '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">'
  },
  {
    id: 3, 
    name: 'Tailwind 1.0.1',
    css: '<link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet">'
  }
]

frameworks.each do |framework_attrs|
  framework = Framework.find_or_initialize_by(id: framework_attrs[:id])
  framework.assign_attributes(framework_attrs)
  framework.save
end

p 'Create templates'
templates = [
  {
    id: 1,
    name: 'Bootstrap base',
    framework_id: 1,
    css: '',
    html: 
'<input class="form-control mb-2" name="nombre" placeholder="Nombre"/>
<input class="form-control mb-2" name="email" placeholder="Email"/>
<textarea class="form-control mb-2" name="mensaje" placeholder="Mensaje"></textarea>
<button class="btn btn-success" type="submit">Enviar</button>',
    js: ''
  },
  {
    id: 2,
    name: 'Bulma base',
    framework_id: 2,
    css: '',
    html: 
'<div class="field">
  <label class="label">Nombre</label>
  <div class="control">
    <input name="nombre" class="input" type="text" placeholder="ej. Alex Smith">
  </div>
</div>
<div class="field">
  <label class="label">Email</label>
  <div class="control">
    <input name="email" class="input" type="email" placeholder="ej. alexsmith@gmail.com">
  </div>
</div>
<div class="field">
  <label class="label">Mensaje</label>
  <div class="control">
    <textarea name="mensaje" class="textarea" placeholder="ej. Hola ...."></textarea>
  </div>
</div>
<button class="button is-success is-rounded" type="submit">Enviar</button>',
    js: ''
  }
]
templates.each do |template_attrs|
  template = Template.find_or_initialize_by(id: template_attrs[:id])
  template.assign_attributes(template_attrs)
  template.save
end

