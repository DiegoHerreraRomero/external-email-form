namespace :maintenance do
  desc "Enable maintenance site"
  task :enable do
    on roles(:all) do
      require 'erb'

      reason = ENV['REASON']
      deadline = ENV['UNTIL']

      result = ERB.new(File.read(File.dirname(__FILE__)+"/../../../app/views/layouts/maintenance.html.erb")).result(binding)

      rendered_path = "#{shared_path}/public/system"
      rendered_name = "maintenance.html"
      rendered_fullpath = "#{rendered_path}/#{rendered_name}"
      
      execute "rm -rf #{shared_path}/public/system"
      execute "mkdir #{rendered_path}"
      upload! StringIO.new(result), rendered_fullpath
      execute "chmod 644 #{rendered_fullpath}"
    end
  end

  desc "Disable maintenance site"
  task :disable do
    on roles(:all) do
      execute "rm -rf #{shared_path}/public/system"
    end
  end
end
