# config valid for current version and patch releases of Capistrano
lock "~> 3.11.0"

set :stages, %w(production staging)
set :default_stage,    :staging

# Change these
server 'diegoherrera.cl', port: 22, roles: [:web, :app, :db], primary: true

set :repo_url,        'git@gitlab.com:DiegoHerreraRomero/external-email-form.git'
set :puma_threads,    [4, 16]
set :puma_workers,    0

# Don't change these unless you know what you're doing
set :pty,             true
set :use_sudo,        false
set :deploy_via,      :remote_cache
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord


# if ENV['skip_assets']=='1'
#   Rake::Task["deploy:assets:precompile"].clear_actions
# end
Rake::Task["deploy:compile_assets"].clear_actions
Rake::Task["deploy:normalize_assets"].clear_actions

## Defaults:
# set :scm,           :git
# set :format,        :pretty
# set :log_level,     :debug
set :keep_releases, 5

## Linked Files & Directories (Default None):
set :linked_files, %w{config/database.yml}
set :linked_dirs,  %w{log tmp/sockets tmp/pids tmp/cache public/uploads}

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/master`
        puts "WARNING: HEAD is not the same as origin/master"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      Rake::Task["maintenance:enable"].clear_actions
      Rake::Task["maintenance:disable"].clear_actions
      Rake::Task["deploy:check:linked_files"].clear_actions
      before 'deploy:symlink:linked_files',     'linked_files:upload_files'
      invoke 'deploy'
    end
  end

  
  desc "Run Seeds on server database"
  task :seed do
   on primary :db do
    within current_path do
      with rails_env: fetch(:stage) do
        execute :rake, 'db:seed'
      end
    end
   end
  end

  before  :starting,    "maintenance:enable"
  before  :starting,    :check_revision
  after   :finishing,   :cleanup
  after   :finishing,   'deploy:assets:precompile'
  after   :finishing,   'deploy:seed'
  after   :finishing,   'puma:restart'
  after   :finishing,   "maintenance:disable"
end

# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma
