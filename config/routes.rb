Rails.application.routes.draw do
  root to: 'home#index'
  
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :web_sites, except: [:index] do 
    member do
      post :send_mail
    end
    collection do
      match :preview, via: [:get, :post]
    end
  end
end
